package com.example.inky.whatstheweather;

import android.content.Context;
import android.hardware.input.InputManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

//The main purpose of this app is to utilize Rest API tools in additon with use of Asyncronous Tasks to deliver current weather information on cities via openweatherAPI.

public class MainActivity extends AppCompatActivity
{
    //Design Elements
    EditText userInput;
    TextView desc;
    String weather;
    StringBuilder sb;


    //The purpose of this task is to fetch weather data based on user input

    public class DownloadCityWeather extends AsyncTask<String,Void,String>
    {
        @Override
        protected String doInBackground(String... strings)
        {
            //This string with return the desciption of the weather
            String result="";

            //We establish a connection like a web browser
            URL url;
            HttpURLConnection conn = null;

            try
            {
                //We're only interested in the first url since this class is feed one url at a time
                url = new URL(strings[0]);

                //We open a connection
                conn = (HttpURLConnection) url.openConnection();
                InputStream in = conn.getInputStream();
                //Input reader is primed
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();

                //Result gets apppended with one char at a time until the end is reached (hence the -1, -1 means its done)
                while (data != -1)
                {
                    char current = (char) data;
                    result+=current;
                    data = reader.read();
                }

                //Close connections for Garbage Collection
                in.close();
                reader.close();
                conn.disconnect();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return "Failed";
            }


            return result;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);

            try
            {

                //JSOn is used to filter the results more easily
                JSONObject jsonObject = new JSONObject(result);
                JSONArray arr = new JSONArray(jsonObject.getString("weather"));
                weather ="";
                JSONObject weatherPart = null;

                //We search for the weather object and append what we want into "weather"
                for(int i = 0; i < arr.length(); i++)
                {
                    weatherPart = arr.getJSONObject(i);
                    weather += weatherPart.getString("main")+": "+ weatherPart.getString("description") + "\n";
                }

                //Weather result is sent to user's screen
                desc.setText(weather);
            }
            catch (Exception e)
            {
                //Toast city not found/ bad data entry
                Toast.makeText(MainActivity.this, "Could not find weather.", Toast.LENGTH_SHORT).show();
            }
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize Design view elements
        userInput = (EditText) findViewById(R.id.editText);
        desc = (TextView) findViewById(R.id.txt_weatherDesctiption);

    }


    //This method is called when the user hits submit and the input is analyzed =
    public void submitCity(View v)
    {
        String city = null;
        try
        {
            city = URLEncoder.encode( userInput.getText().toString(), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            //Default error message for user
            Toast.makeText(MainActivity.this, "Could not find weather.", Toast.LENGTH_SHORT).show();

            //More errors like bad input will be handled by other exceptions
        }


        sb  = new StringBuilder();
        //Main URL that's called
        sb.append("http://api.openweathermap.org/data/2.5/weather?q=");
        sb.append(city);
        //Key to grant api access
        sb.append("&&APPID=a059c97a5ffc910ca81e234ee45fda8d");

        //This is how you hide the keyboard
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(userInput.getWindowToken(),0);


        //We get our data
        DownloadCityWeather dcw = new DownloadCityWeather();
        dcw.execute(sb.toString());


    }
}
